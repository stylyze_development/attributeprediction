import csv
import numpy as np
import pandas as pd
import pickle
import re
import os

import vsnm.evaldata3styletarget as data
import vsnm.eval3 as model
import vsnm.configuration3 as configuration
import vsnm.tojson3style as tojson

# TODO: implement setup.py/requirement.txt and package all the
# modulues used here


def main():   

    args = configuration.parse_arguments()

    output_csv = pd.DataFrame({"CATEGORY": ["DUMMY"], "OBJECT_ID": ["DUMMY"], "URL": ["DUMMY"], "STYLE": [
                              "DUMMY"], "STYLE PREDICTION": ["DUMMY"], "STYLE OTHER ": ["DUMMY"], "USER ID ": ["DUMMY"]})

    item_counter = 1
    # list_groups = ["Rugs"]
    # list_groups = ["type"]
    list_groups = ["patterns"]

    for lsb_type in list_groups:

        # n_y, feature_dict_short = data.load_data("/data_tables/VISUALLY_SIMILAR_TARGET_QAAPPROVED_QAUNAPPROVED_EXTENDED_08292019.csv",
        #                                          args["output_table_path"],
        #                                          args["column_names"],
        #                                          lsb_type,
        #                                          item_counter)

        n_y, feature_dict_short = data.load_data(args["database_path"],
                                                 args["output_table_path"],
                                                 args["column_names"],
                                                 lsb_type,
                                                 item_counter)

        # model.embedding(args["output_table_path"],
        #                 args["n_H0"],
        #                 args["n_W0"],
        #                 args["n_C0"],
        #                 args["batch_size"],
        #                 args["training_ratio"],
        #                 args["embedding_size"],
        #                 args["image_normalization"],
        #                 args["same_image_epsilon"],
        #                 args["model_weight_path"],
        #                 args["model_weight_path_2"],
        #                 args["model_name"],
        #                 args["output_layer"],
        #                 args["num_epoch"],
        #                 args["MARGIN"],
        #                 args["net_3_dim"],
        #                 args["learning_rate"],
        #                 args["keepratio"],
        #                 n_y,
        #                 feature_dict_short,
        #                 lsb_type)

        csv_path = model.embedding(args["output_table_path"],
                                   args["n_H0"],
                                   args["n_W0"],
                                   args["n_C0"],
                                   args["batch_size"],
                                   args["training_ratio"],
                                   args["embedding_size"],
                                   args["image_normalization"],
                                   args["same_image_epsilon"],
                                   args["model_weight_path"],
                                   args["model_weight_path_2"],
                                   args["model_name"],
                                   args["output_layer"],
                                   args["num_epoch"],
                                   args["MARGIN"],
                                   args["net_3_dim"],
                                   args["learning_rate"],
                                   args["keepratio"],
                                   n_y,
                                   feature_dict_short,
                                   lsb_type)

        # csv_path = cwd + "/weights/" + "home_deport_pred/" + lsb_type + "/style_pred_score.csv"

        output_csv_i = pd.read_csv(csv_path)
        output_csv_i = output_csv_i.drop(["Unnamed: 0"], axis=1)
        output_csv = pd.concat([output_csv, output_csv_i], ignore_index=True)
        item_counter += 1

    cwd = os.getcwd()
    output_csv = output_csv[output_csv["STYLE"] != "DUMMY"]
    output_csv.to_csv(cwd + "/weights/" +
                      "target_pred/" + "style_pred_score_all.csv", index=False)

    path = cwd + "/weights/" + "target_pred/" + "style_pred_score_all.csv"
    path_2 = cwd + "/weights/" + "target_pred/" + "style_pred_score_all_category.csv"
    tojson.output_json(args["output_table_path_3"], args["output_table_path_14"], path, path_2)

if __name__ == '__main__':
    main()
