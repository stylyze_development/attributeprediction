import csv
import numpy as np
import pandas as pd
import pickle
import re

import vsnm.traindata3styletarget as data
# import vsnm.traindata3styletarget2 as data
import vsnm.configuration3 as configuration
import vsnm.traindataaug3 as augmentation
import vsnm.train3 as model
# import vsnm.trainsoftmax3 as model

# TODO: implement setup.py/requirement.txt and package all the
# modulues used here


def main():

    args = configuration.parse_arguments()

    # augmentation.data_aug(args["database_path"], args["database_path_aug"])

    item_counter = 1
    # list_groups = ["Rugs"]
    list_groups = ["patterns"]
     
    for lsb_type in list_groups:

        # n_y, feature_dict_short, class_stats = data.load_data(args["database_path_aug"],
        #                                                       args[
        #                                                           "output_table_path"],
        #                                                       args[
        #                                                           "column_names"],
        #                                                       lsb_type,
        #                                                       item_counter)

        n_y, feature_dict_short, class_stats = data.load_data(args["database_path"],
                                                              args[
                                                                  "output_table_path"],
                                                              args[
                                                                  "column_names"],
                                                              lsb_type,
                                                              item_counter)

        model.embedding(args["output_table_path"],
                        args["n_H0"],
                        args["n_W0"],
                        args["n_C0"],
                        args["batch_size"],
                        args["training_ratio"],
                        args["embedding_size"],
                        args["image_normalization"],
                        args["same_image_epsilon"],
                        args["model_weight_path"],
                        args["model_weight_path_2"],
                        args["model_name"],
                        args["output_layer"],
                        args["num_epoch"],
                        args["MARGIN"],
                        args["net_3_dim"],
                        args["learning_rate"],
                        args["keepratio"],
                        n_y,
                        feature_dict_short,
                        lsb_type,
                        class_stats)

        item_counter += 1

if __name__ == '__main__':
    main()
