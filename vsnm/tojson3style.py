import numpy as np
import os
import pandas as pd
import pickle
import sys
import json
import csv
import logging
import re


def output_json(output_table_path_3, output_table_path_14, path, path_2):

    cwd = os.getcwd()
    the_csv = pd.read_csv(path)
    # the_csv = the_csv.drop(["Unnamed: 0"], axis=1)
    # the_csv = the_csv.head(15)
    the_csv = the_csv.fillna("Missing") 
    style_pred = {}
    style_pred["machineName"] = "tfs"
    style_pred["accountName"] = "target"
    style_pred["executionDate"] = "2020-09-11"

    lsbResultMap = []    
    for i in range(len(the_csv)):
        
        id_score_i = {} 
        id_score_wrapper_i = {}    
        variant_id = str(the_csv["OBJECT_ID"].loc[i])
        id_score_i["lsbId"] = variant_id
        
        item = the_csv["STYLE PREDICTION"].loc[i]
        styleForType = []
        if item != "Missing":
            item_i = item.replace(" ", "")
            item_i = re.split('[,]', item)
            for j in range(int(len(item_i)/2)):
                value_logit_i = {}
                value_logit_i["value"] = item_i[2*j].lstrip() 
                value_logit_i["confidence"] = item_i[2*j+1].replace(" ", "")
                styleForType.append(value_logit_i)
                # print(styleForType)
        elif item == "Missing":
            value_logit_i = {}
            value_logit_i["value"] = ""
            value_logit_i["confidence"] = ""

            styleForType.append(value_logit_i) 
            
        id_score_i["mlResults"] = {"stylesForType": styleForType} 

        lsbResultMap.append(id_score_i)  

    style_pred["lsbResultsMap"] = lsbResultMap

    with open(cwd + "/output_tables/target_style_pred.json", 'w') as f:
        json.dump(style_pred, f)

    # Output file path of id.csv
    # id.csv is the output file to the backend team
    # output_csv_path = `python evaluation.py input_csv_path`
    sys.stdout.write(cwd + "/output_tables/target_style_pred.json")
    sys.exit(0)
