import csv
import numpy as np
import logging
import os
import pandas as pd
import pickle
import re
import cv2
from datetime import datetime


def data_aug(database_path, database_path_aug):

    cwd = os.getcwd()

    deep_learning_df = pd.read_csv(cwd + database_path)

    # print(deep_learning_df.head(5))

    deep_learning_df_aug = deep_learning_df.copy(deep=True)

    df_big = deep_learning_df.copy(deep=True)

    df_big = df_big[(df_big["CATEGORIES"] == "Blouses") | (df_big["CATEGORIES"] == "Tank Tops")] 

    # df_big = df_big[(df_big["CATEGORIES"] == "tops") | (df_big["CATEGORIES"] == "dresses") | (df_big["CATEGORIES"] == "bags") |
    #  (df_big["CATEGORIES"] == "pants") | (df_big["CATEGORIES"] == "jackets")]
 
    # df_big = df_big.fillna("Missing") 

    # df_big = df_big[df_big["PATTERNS"] != "Missing"]

    bad_img = 0

    i = 0
    startTime = datetime.now()
    # for item in deep_learning_df["OBJECT_ID"][:30000]:
    for item in df_big["OBJECT_ID"]:

        img_path = deep_learning_df_aug[deep_learning_df_aug[
            "OBJECT_ID"] == item]["CURATOR_IMAGE_PATH"].values[0]

        img_0 = cv2.imread(img_path) 
        img_1 = cv2.imread(img_path) 
        img_2 = cv2.imread(img_path)  

        if img_0 is not None:
                 
            bad_img += 0
            #############################################################################
            # print(img_path)
            
            # fimg_0 = img_0[10:200, 10:200]
            fimg_0 = cv2.flip(img_0, 1)
             
            image_path_aug = "./target_augmentation/"+ item + "_flipped" + ".jpg" 
            cv2.imwrite("./target_augmentation/"+ item + "_flipped" + ".jpg", fimg_0)
            items = deep_learning_df_aug[
                deep_learning_df_aug["OBJECT_ID"] == item]

            df_cropped = items.copy(deep=True)
            df_cropped["OBJECT_ID"] = item + "_flipped"
            df_cropped["CURATOR_IMAGE_PATH"] = image_path_aug
            deep_learning_df_aug = deep_learning_df_aug.append(
                df_cropped, ignore_index=True)

            #############################################################################
            # print(img_path)
            
            (h, w) = img_1.shape[:2]
            center = (w / 2, h / 2)
            scale = 1.0
            degree = 90
            M = cv2.getRotationMatrix2D(center, degree, scale)
            fimg_1 = cv2.warpAffine(img_1, M, (h, w))

            image_path_aug = "./target_augmentation/"+ item + "_rotated" + ".jpg" 
            cv2.imwrite("./target_augmentation/"+ item + "_rotated" + ".jpg", fimg_1)
            items = deep_learning_df_aug[
                deep_learning_df_aug["OBJECT_ID"] == item]

            df_rotated = items.copy(deep=True)
            df_rotated["OBJECT_ID"] = item + "_rotated"
            df_rotated["CURATOR_IMAGE_PATH"] = image_path_aug
            deep_learning_df_aug = deep_learning_df_aug.append(
                df_rotated, ignore_index=True)

            #############################################################################
            # print(img_path)

            seed = np.random.randint(3)
            h,w,c = img_2.shape  
            noise = np.random.randint(0,50,(h, w)) # design jitter/noise here
            zitter = np.zeros_like(img_2)
            zitter[:,:,seed] = noise  
            fimg_2 = cv2.add(img_2, zitter)

            image_path_aug = "./target_augmentation/"+ item + "_color_jitter" + ".jpg" 
            cv2.imwrite("./target_augmentation/"+ item + "_color_jitter" + ".jpg", fimg_2)
            items = deep_learning_df_aug[
                deep_learning_df_aug["OBJECT_ID"] == item]

            df_color_jitter = items.copy(deep=True)
            df_color_jitter["OBJECT_ID"] = item + "_color_jitter"
            df_color_jitter["CURATOR_IMAGE_PATH"] = image_path_aug
            deep_learning_df_aug = deep_learning_df_aug.append(
                df_color_jitter, ignore_index=True)

            time_used = datetime.now() - startTime
            if i % 600 == 0:
                print("IMAGE " + str(i + 1) + " IS FINISHED")
                print("TIME TAKEN:", time_used)

            i += 1

        else:
            bad_img += 1
            print("BAD IMAGE: " + item)
                

    # with open("./output_tables/deep_learning_df_aug", "wb") as my_file:
    #     pickle.dump(deep_learning_df_aug, my_file, protocol=2)

    print("TOTAL NUMBER OF BAD IMAGES: " + str(bad_img))
    deep_learning_df_aug.to_csv(
        cwd + database_path_aug, index=False)
