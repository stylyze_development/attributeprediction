import csv
import cv2
from datetime import datetime
import logging
import numpy as np
import os
import pandas as pd
import pickle
import scipy.io
import tensorflow as tf

from . import generator3 as generator
from . import models3 as models


def embedding(model_input_path_2,
              n_H0, n_W0, n_C0,
              batch_size, training_ratio, embedding_size,
              image_normalization,
              same_image_epsilon,
              model_weight_path,
              model_weight_path_2,
              model_name,
              output_layer,
              num_epoch,
              MARGIN,
              net_3_dim,
              learning_rate,
              keepratio,
              n_y,
              feature_dict_short,
              lsb_type,
              class_stats):
    """

    """

    cwd = os.getcwd()

    with open(cwd + "/output_tables/y_index", "rb") as my_file:
        y_index = pickle.load(my_file)

    with open(cwd + model_input_path_2, "rb") as my_file2:
        deep_learning_df = pickle.load(my_file2)

    with open(cwd + "/output_tables/labels", "rb") as my_file3:
        labels = pickle.load(my_file3)

    image_path = labels["img_path"]

    # indices = np.random.permutation(6400)
    # y_index = y_index[indices]

    index = y_index[:, 0]
    y_labels = y_index[:, 1:]

    type(y_index[:, 1:])
    type(y_index[0, 1:])
    type(y_index[0, 1])

    print("NUMBER OF SAMPLES:", len(y_labels))
    print("NUMBER OF CLASSES:", len(y_labels[0]))

    print("*******************************************")
    # print("***TOTAL NUMBER OF LSBS:" + str(len(y_labels)))

    grouped = deep_learning_df

    num_epoch = 200
    batch_size = 32
    training_ratio = 0.9
    n_H0, n_W0, n_C0 = (224, 224, 3)
    net_3_dim = net_3_dim
    learning_rate = 0.0001
    # keepratio = keepratio

    # Parameters
    params = {'dim_x': n_W0,
              'dim_y': n_H0,
              'dim_z': n_C0,
              'batch_size': batch_size,
              'n_y': n_y,
              'shuffle': False}

    img_placeholder = tf.placeholder(tf.float32, shape=(
        None, n_H0, n_W0, n_C0), name="img_placeholder")

    # Construct the new model
    model_weight_path_update = cwd + model_weight_path

    net_val, mean_pixel = models.vgg19_no_top(
        model_weight_path_update, img_placeholder)

    train_features_tf = tf.identity(
        # net_val['relu5_4'], name="train_features_tf")
        net_val['pool5'], name="train_features_tf")

    n_input = n_H0 * n_W0 * n_C0
    n_output = n_y

    x = tf.placeholder(tf.float32, [None, 7 * 7 * 512], name="x")
    # y = tf.placeholder(tf.int32, [None, n_output], name="y")
    y = tf.placeholder(tf.float32, [None, n_output], name="y")
    keepratio = tf.placeholder(tf.float32, name="keepratio")

    # weights = {
    #     'wd1': tf.Variable(tf.random_normal([7 * 7 * 512, net_3_dim], stddev=0.1), name="wd1"),
    #     'wd2': tf.Variable(tf.random_normal([net_3_dim, n_output], stddev=0.1), name="wd2")
    # }
    # biases = {
    #     'bd1': tf.Variable(tf.random_normal([net_3_dim], stddev=0.1), name="bd1"),
    #     'bd2': tf.Variable(tf.random_normal([n_output], stddev=0.1), name="bd2")
    # }

    weights = {
        "wd1": tf.get_variable("wd1", [7 * 7 * 512, net_3_dim]),
        # "wd2": tf.get_variable("wd2", [net_3_dim, n_output])
        "wd2": tf.get_variable("wd2", [net_3_dim, net_3_dim]),
        "wd3": tf.get_variable("wd3", [net_3_dim, n_output])     
    }
    biases = {
        "bd1": tf.get_variable("bd1", [net_3_dim]),
        # "bd2": tf.get_variable("bd2", [n_output])
        "bd2": tf.get_variable("bd2", [net_3_dim]),
        "bd3": tf.get_variable("bd3", [n_output])     
    }     

    # weights = {
    #     "wd1": tf.get_variable(
    #     "wd1", [7 * 7 * 512, net_3_dim], initializer=tf.contrib.layers.xavier_initializer(seed=0)),
    #     "wd2": tf.get_variable(
    #     "wd2", [net_3_dim, n_output], initializer=tf.contrib.layers.xavier_initializer(seed=0))     
    # }
    # biases = {
    #     "bd1": tf.get_variable(
    #     "bd1", [net_3_dim], initializer=tf.contrib.layers.xavier_initializer(seed=0)),
    #     "bd2": tf.get_variable(
    #     "bd2", [n_output], initializer=tf.contrib.layers.xavier_initializer(seed=0))     
    # }     

    def conv_basic(_input, _w, _b, _keepratio):
        # Input
        _input_r = _input
        # Vectorize
        _dense1 = tf.reshape(
            _input_r, [-1, _w['wd1'].get_shape().as_list()[0]])
        # Fc1
        _fc1 = tf.nn.relu(tf.add(tf.matmul(_dense1, _w['wd1']), _b['bd1']))
        _fc_dr1 = tf.nn.dropout(_fc1, _keepratio)
        # Fc2
        _fc_dr2 = tf.nn.relu(tf.add(tf.matmul(_fc_dr1, _w['wd2']), _b['bd2']))
        _out0 = tf.nn.dropout(_fc_dr2, _keepratio)
        _out = tf.add(tf.matmul(_out0, _w['wd3']), _b['bd3'])
        # _out = tf.add(tf.matmul(_fc_dr1, _w['wd2']), _b['bd2'])
        # Return everything
        # out = {'input_r': _input_r, 'dense1': _dense1,
        # 'fc1': _fc1, 'out': _out}
        out = {'input_r': _input_r, 'dense1': _dense1,
               'fc1': _fc1, 'fc_dr1': _fc_dr1, 'out': _out}
        return out

    # weights = {
    #     "wd1": tf.get_variable(
    #     "wd1", [7 * 7 * 512, net_3_dim], initializer=tf.contrib.layers.xavier_initializer(seed=0)),
    #     "wd2": tf.get_variable(
    #     "wd2", [net_3_dim, n_output], initializer=tf.contrib.layers.xavier_initializer(seed=0))     
    # }
    # biases = {
    #     "bd1": tf.get_variable(
    #     "bd1", [net_3_dim], initializer=tf.contrib.layers.xavier_initializer(seed=0)),
    #     "bd2": tf.get_variable(
    #     "bd2", [n_output], initializer=tf.contrib.layers.xavier_initializer(seed=0))     
    # }     

    # # weights = {
    # #     'wd1': tf.Variable(tf.random_normal([7 * 7 * 512, net_3_dim], stddev=0.1), name="wd1"),
    # #     'wd2': tf.Variable(tf.random_normal([net_3_dim, net_3_dim], stddev=0.1), name="wd2")
    # # }
    # # biases = {
    # #     'bd1': tf.Variable(tf.random_normal([net_3_dim], stddev=0.1), name="bd1"),
    # #     'bd2': tf.Variable(tf.random_normal([net_3_dim], stddev=0.1), name="bd2")
    # # }

    # def conv_basic(_input, _w, _b, keepratio_tf):
    #     # Input
    #     _input_r = _input
    #     # Vectorize
    #     _dense1 = tf.reshape(
    #         _input_r, [-1, _w['wd1'].get_shape().as_list()[0]])
    #     # Fc1
    #     _fc1 = tf.nn.relu(tf.add(tf.matmul(_dense1, _w['wd1']), _b['bd1']))
    #     _fc_dr1 = tf.nn.dropout(_fc1, keepratio_tf)
    #     # _out = tf.nn.dropout(_fc1, keepratio_tf)
    #     # Fc2
    #     _fc2 = tf.nn.relu(tf.add(tf.matmul(_fc_dr1, _w['wd2']), _b['bd2']))
    #     _out = tf.nn.dropout(_fc2, keepratio_tf)
    #     # out = {'input_r': _input_r, 'dense1': _dense1,
    #     # 'fc1': _fc1, 'out': _out}
    #     out = {'input_r': _input_r, 'dense1': _dense1,
    #            'fc1': _fc1, 'fc_dr1': _fc_dr1, 'out': _out}
    #     return out    

    # pos_weight = [1 / 0.14189983, 1 / 0.0550733, 1 / 0.13870389, 1 / 0.23348935, 1 /
    # 0.37444071, 1 / 0.09492979, 1 / 0.04548547, 1 / 0.1487041, 1 /
    # 0.1858595, 1 / 0.11128064]

    # pos_weight_sum = 1 / 0.14189983 + 1 / 0.0550733 + 1 / 0.13870389 + 1 / 0.23348935 + 1 / \
    #     0.37444071 + 1 / 0.09492979 + 1 / 0.04548547 + \
    #     1 / 0.1487041 + 1 / 0.1858595 + 1 / 0.11128064

    _pred = conv_basic(x, weights, biases, keepratio)['out']

    # cost = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
    # _sentinel=None,
    # labels=tf.argmax(y, 1),
    # logits=_pred,
    # name=None))

    # unweighted_losses = tf.nn.softmax_cross_entropy_with_logits(
    #          logits=_pred, labels=y) 
    # # class_weights = tf.constant([[10.0, 2.5, 2.0]])
    # inv_class_stats = []
    # for class_stats_i in class_stats:
    #     inv_class_stats.append(1/class_stats_i)
    # print(class_stats)
    # print(inv_class_stats)
    # class_weights  = tf.constant([inv_class_stats])
    # weights = tf.reduce_sum(class_weights * y, axis=1)
    # weighted_losses = unweighted_losses * weights
    # cost = tf.reduce_mean(weighted_losses)

    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(
             logits=_pred, labels=y))
    optm = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
    class_dist = tf.nn.softmax(_pred, name="class_dist")
    pred_class = tf.cast(tf.argmax(class_dist, 1), tf.int32)
    Z33 = tf.to_float(pred_class, name = "Z33")
    n_y = 1
    y_class = tf.cast(tf.argmax(y, 1), tf.int32)
    correct_prediction_2 = tf.equal(pred_class, y_class, name = "correct_prediction_2")
    accuracy_2 = tf.reduce_mean(tf.cast(correct_prediction_2, "float"), name="accuracy_2")

    print(class_dist.get_shape())
    print(Z33.get_shape())

    # cost = tf.reduce_mean(tf.nn.weighted_cross_entropy_with_logits(
    #     logits=_pred, targets=y, pos_weight=1))
    # optm = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
    # class_dist = tf.sigmoid(_pred, name="class_dist")
    # # Z3 = tf.round(tf.sigmoid(_pred))
    # Z33 = tf.to_float(tf.sigmoid(_pred) > 0.2, name="Z33")
    # # Z33 = (tf.sigmoid(_pred))
    # # correct_prediction = tf.equal(Z3, y)
    # correct_prediction_2 = tf.equal(Z33, y, name="correct_prediction_2")
    # # accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"), 0)
    # accuracy_2 = tf.reduce_mean(tf.cast(correct_prediction_2, "float"))

    # print(cost.get_shape())
    # print(_pred.get_shape())
    # print(y.get_shape())

    # cost_shape = tf.shape(cost)
    # y_shape = tf.shape(y)

    # a = _pred
    # b = -tf.abs(_pred)
    # c = tf.exp(-tf.abs(_pred))
    # d = (1 + tf.exp(-tf.abs(_pred)))
    # e = tf.log(1 + tf.exp(-tf.abs(_pred)))
    # f = y
    # g = _pred * y
    # h = tf.maximum(_pred, -1)

    # cost_2 = tf.reduce_mean(tf.maximum(_pred, 0) -
    #                         _pred * y + tf.log(1 + tf.exp(-tf.abs(_pred))))

    total = 0
    total_cost = 0

    number_of_products = len(y_labels)

    m = number_of_products

    y_labels_training = {}
    partition_training = []
    index_training = []
    for i in range(0, int(number_of_products * training_ratio)):
        index_i = int(index[i])
        y_labels_training[str(index_i)] = y_labels[i, :]
        partition_training.append(str(index_i))
        index_training.append(index_i)

    # print(y_labels_training)

    y_labels_testing = {}
    partition_testing = []
    index_testing = []
    for i in range(int(number_of_products * training_ratio), number_of_products):
        index_i = int(index[i])
        y_labels_testing[str(index_i)] = y_labels[i, :]
        partition_testing.append(str(index_i))
        index_testing.append(index_i)

    # print(y_labels_testing)

    # print(y_labels_testing["44753"])
    # print(y_labels_testing["44783"])
    # print(y_labels_testing["44785"])
    # print(y_labels_testing["44803"])
    # print(y_labels_testing["44809"])

    training_generator = generator.DataGenerator(
        **params).generate(y_labels_training, partition_training, image_path)

    testing_generator = generator.DataGenerator(
        **params).generate(y_labels_testing, partition_testing, image_path)

    minibatch_size = batch_size
    num_minibatches_training = np.int(
        np.round(m * training_ratio) / minibatch_size)
    num_minibatches_testing = np.int(
        np.round(m * (1 - training_ratio)) / minibatch_size)

    # print(num_minibatches_testing)
    print("NUMBER OF SAMPLES (TRAINING):", len(index_training))
    print("NUMBER OF SAMPLES (TESTING):", len(index_testing))

    init = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init)

        print("*******************************************")
        print("***TRAINING BEGINS...")
        startTime = datetime.now()
        for epoch in range(num_epoch):
            group_i = grouped
            if len(group_i) >= 1:

                minibatches_training = training_generator

                minibatch_cost = 0

                minibatch_cost_2 = 0

                train_acc = 0

                total_bad_imgs = 0

                # matrix_res = np.zeros(
                #     [np.int(np.round(m * training_ratio)), 7 * 7 * 512])

                # prediction_train = np.zeros(
                #     (np.int(np.round(m * (training_ratio))), n_y))
                # y_train_true = np.zeros(
                #     (np.int(np.round(m * (training_ratio))), n_y))
                for i in range(num_minibatches_training):
                    (minibatch_X_training, minibatch_Y_training,
                     bad_img) = next(minibatches_training)

                    total_bad_imgs += np.sum(bad_img)

                    train_features = net_val['pool5'].eval(
                        feed_dict={img_placeholder: minibatch_X_training})

                    ntrain = batch_size
                    train_vectorized = np.ndarray((ntrain, 7 * 7 * 512))

                    for j in range(ntrain):
                        curr_feat = train_features[j, :, :, :]
                        curr_feat_vec = np.reshape(curr_feat, (1, -1))
                        train_vectorized[j, :] = curr_feat_vec

                    # if epoch == num_epoch - 1:
                    #     matrix_res[
                    #         i * batch_size:(i + 1) * batch_size, :] = train_vectorized

                    batch_xs = train_vectorized
                    batch_ys = minibatch_Y_training
                    # Fit training using batch data
                    sess.run(optm, feed_dict={
                             x: batch_xs, y: batch_ys, keepratio: 0.5})
                    # Compute average loss
                    # minibatch_cost += sess.run(
                    # class_cross_entropy, feed_dict={x: batch_xs, y: batch_ys,
                    # keepratio: 1.}) / num_minibatches_training

                    minibatch_cost += sess.run(
                        cost, feed_dict={x: batch_xs, y: batch_ys, keepratio: 1.}) / num_minibatches_training

                    # prediction_train[i * batch_size:(i + 1) * batch_size, :] = sess.run(
                    #     Z33, feed_dict={x: batch_xs, y: batch_ys, keepratio: 1.})

                    # y_train_true[
                    #     i * batch_size:(i + 1) * batch_size, :] = batch_ys

                    train_acc += sess.run(accuracy_2, feed_dict={
                        x: batch_xs, y: batch_ys, keepratio: 1.}) / num_minibatches_training

                    # aa = sess.run(
                    # a, feed_dict={x: batch_xs, y: batch_ys, keepratio: 1.})

                    # bb = sess.run(
                    # b, feed_dict={x: batch_xs, y: batch_ys, keepratio: 1.})

                    # cc = sess.run(
                    # c, feed_dict={x: batch_xs, y: batch_ys, keepratio: 1.})

                    # dd = sess.run(
                    # d, feed_dict={x: batch_xs, y: batch_ys, keepratio: 1.})

                    # ee = sess.run(
                    # e, feed_dict={x: batch_xs, y: batch_ys, keepratio: 1.})

                    # ff = sess.run(
                    # f, feed_dict={x: batch_xs, y: batch_ys, keepratio: 1.})

                    # gg = sess.run(
                    # g, feed_dict={x: batch_xs, y: batch_ys, keepratio: 1.})

                    # hh = sess.run(
                    # h, feed_dict={x: batch_xs, y: batch_ys, keepratio: 1.})

                    # costt_shape = sess.run(
                    # cost_shape, feed_dict={x: batch_xs, y: batch_ys,
                    # keepratio: 1.})

                    # yy_shape = sess.run(
                    # y_shape, feed_dict={x: batch_xs, y: batch_ys, keepratio:
                    # 1.})

                # if epoch == num_epoch - 1:
                if epoch == epoch:
                    print("***TRAINING RESULTS:")
                    print("ACCURACY: " + str(train_acc))
                    print("COST: " + str(minibatch_cost))

                    # SCORE_TRAIN = []
                    # for i in range(len(index_training)):
                    #     # num_items = sum(y_test_true[i])
                    #     num_items = 0
                    #     score_i = 0
                    #     for j in range(len(y_labels[0])):
                    #         if y_train_true[i][j] == 1 and prediction_train[i][j] == 1:
                    #             num_items += 1
                    #             score_i += 1
                    #         elif y_train_true[i][j] == 0 and prediction_train[i][j] == 1:
                    #             num_items += 1
                    #         elif y_train_true[i][j] == 1 and prediction_train[i][j] == 0:
                    #             num_items += 1
                    #     score_i = score_i / num_items

                    #     SCORE_TRAIN.append(round(score_i, 3))

                    # AVG_SCORE_TRAIN = np.mean(SCORE_TRAIN)
                    # print("SCORE: " + str(AVG_SCORE_TRAIN))
                    print("# OF BAD IMAGES (TRAINING):" +
                          " " + str(int(total_bad_imgs)))

                    class_stats = (np.sum(y_labels[0:int(number_of_products * training_ratio), :], axis=0)) / len(
                    y_labels[0:int(number_of_products * training_ratio), :])

                    # print(len(y_labels[0:int(number_of_products * training_ratio), :]))
                    # print(class_stats)
                    # print(np.sum(class_stats))

                    # print(minibatch_cost_2)
                    # print(aa)
                    # print(bb)
                    # print(cc)
                    # print(dd)
                    # print(ee)
                    # print(ff)
                    # print(gg)
                    # print(hh)
                    # print(yy_shape)
                    # print(costt_shape)

                    # print(y_train_true[0])
                    # print(prediction_train[0])
                    # print(y_train_true[1])
                    # print(prediction_train[1])
                    # print(y_train_true[2])
                    # print(prediction_train[2])
                    # print(y_train_true[3])
                    # print(prediction_train[3])
                    # print(y_train_true[4])
                    # print(prediction_train[4])
                    # print(y_train_true[5])
                    # print(prediction_train[5])

            # matrix_res_test = np.zeros(
            #     [np.int(np.round(m * (1 - training_ratio))), 7 * 7 * 512])
            # prediction = np.zeros(
            #     (np.int(np.round(m * (1 - training_ratio))), n_y))
            # y_test_true = np.zeros(
            #     (np.int(np.round(m * (1 - training_ratio))), n_y))

            # correct_2 = np.zeros(
            #     (np.int(np.round(m * (1 - training_ratio))), n_y))

            # class_dist_np = np.zeros(
            #     (np.int(np.round(m * (1 - training_ratio))), n_y))

            total_bad_imgs_test = 0

            test_acc = 0
            if 1 == 1:

                minibatches_testing = testing_generator

                minibatch_cost_testing = 0

                for i in range(num_minibatches_testing):

                    (minibatch_X_testing, minibatch_Y_testing, bad_img) = next(
                        minibatches_testing)

                    total_bad_imgs_test += np.sum(bad_img)

                    test_features = net_val['pool5'].eval(
                        feed_dict={img_placeholder: minibatch_X_testing})

                    ntest = batch_size
                    test_vectorized = np.ndarray((ntest, 7 * 7 * 512))

                    for j in range(ntest):
                        curr_feat = test_features[j, :, :, :]
                        curr_feat_vec = np.reshape(curr_feat, (1, -1))
                        test_vectorized[j, :] = curr_feat_vec

                    # matrix_res_test[
                    #     i * batch_size:(i + 1) * batch_size, :] = test_vectorized

                    batch_xs = test_vectorized
                    batch_ys = minibatch_Y_testing

                    # prediction[i * batch_size:(i + 1) * batch_size, :] = sess.run(
                    #     Z33, feed_dict={x: batch_xs, y: batch_ys, keepratio: 1.})
                    # y_test_true[i * batch_size:(i + 1) * batch_size, :] = batch_ys

                    test_acc += sess.run(accuracy_2, feed_dict={
                                         x: batch_xs, y: batch_ys, keepratio: 1.}) / num_minibatches_testing

                    # minibatch_cost_testing += sess.run(
                    # class_cross_entropy, feed_dict={x: batch_xs, y: batch_ys,
                    # keepratio: 1.}) / num_minibatches_testing

                    minibatch_cost_testing += sess.run(
                        cost, feed_dict={x: batch_xs, y: batch_ys, keepratio: 1.}) / num_minibatches_testing

                    # correct_2[i * batch_size:(i + 1) * batch_size, :] = sess.run(
                    #     correct_prediction_2, feed_dict={x: batch_xs, y: batch_ys, keepratio: 1.})

                    # class_dist_np[i * batch_size:(i + 1) * batch_size, :] = sess.run(
                    #     class_dist, feed_dict={x: batch_xs, y: batch_ys, keepratio: 1.})

                    # check = sess.run(
                    # correct_prediction_2, feed_dict={x: batch_xs, y: batch_ys,
                    # keepratio: 1.})

                # correct_2_mean = np.mean(correct_2, axis=0)

                # test_acc_estimate = np.mean(
                #     (np.array(prediction) == np.array(y_test_true)).astype(float), axis=0)

                # binary_vector = (np.array(prediction) ==
                #                  np.array(y_test_true)).astype(float)

                # class_stats = (np.sum(y_test_true, axis=0)) / len(y_test_true)

                print("***TESTING RESULTS:")
                # print(len(check))
                # print(i)
                # print(len(correct_2))
                # print(len(prediction))
                # print(len(binary_vector))

                # print(np.sum(test_acc_estimate) / len(y_labels[0]))
                # print(np.sum(correct_2_mean) / len(y_labels[0]))

                print("ACCURACY: " + str(test_acc))
                print("COST: " + str(minibatch_cost_testing))

                # print(correct_2_mean)
                # print(class_stats)
                # print("***************************************")
                # print(y_test_true[0])
                # print(prediction[0])
                # print("***************************************")
                # print(y_test_true[1])
                # print(prediction[1])
                # print("***************************************")
                # print(y_test_true[2])
                # print(prediction[2])
                # print("***************************************")
                # print(y_test_true[3])
                # print(prediction[3])
                # print("***************************************")
                # print(y_test_true[4])
                # print(prediction[4])
                # print("***************************************")
                # print(y_test_true[5])
                # print(prediction[5])
                # print("***************************************")
                # print(y_test_true[6])
                # print(prediction[6])
                # print("***************************************")
                # print(y_test_true[7])
                # print(prediction[7])
                # print("***************************************")
                # print(y_test_true[8])
                # print(prediction[8])
                # print("***************************************")
                # print(y_test_true[9])
                # print(prediction[9])

            # SCORE = []
            # NULL = []
            # for i in range(len(index_testing)):
            #     # num_items = sum(y_test_true[i])
            #     num_items = 0
            #     score_i = 0
            #     for j in range(len(y_labels[0])):
            #         if y_test_true[i][j] == 1 and prediction[i][j] == 1:
            #             num_items += 1
            #             score_i += 1
            #         elif y_test_true[i][j] == 0 and prediction[i][j] == 1:
            #             num_items += 1
            #         elif y_test_true[i][j] == 1 and prediction[i][j] == 0:
            #             num_items += 1
            #     score_i = score_i / num_items

            #     SCORE.append(round(score_i, 3))

            #     if np.sum(prediction[i]) == 0:
            #         NULL.append(1)
            #     else:
            #         NULL.append(0)

            # AVG_NULL = np.mean(NULL)    
            # AVG_SCORE = np.mean(SCORE)
            # print("SCORE: " + str(AVG_SCORE))
            # print("NULL PREDICTIONS: " + str(AVG_NULL))
            print("# OF BAD IMAGES (TESTING):" +
                          " " + str(int(total_bad_imgs_test)))

            time_used = datetime.now() - startTime
            print("EPOCH:" + " " + str(epoch))
            print("TIME TAKEN:", time_used)
            print("***************************************")
            # print(len(index_testing))

        # INDEX = []
        # STYLE = []
        # URL = []
        # ID = []
        # STYLE_PRED = []
        # CLASS = []
        # for i in range(len(index_testing)):
        #     index_i = index_testing[i]
        #     INDEX.append(index_i)
        #     style_i = deep_learning_df.loc[index_i]["STYLES"]
        #     STYLE.append(style_i)
        #     url_i = deep_learning_df.loc[index_i]["IMAGE_URL"]
        #     URL.append(url_i)
        #     id_i = deep_learning_df.loc[index_i]["OBJECT_ID"]
        #     ID.append(id_i)

        #     CLASS.append([int(elem*100)
        #                   for elem in class_dist_np[i].tolist()])

        #     style_pred_i = []

        #     for j in range(len(y_labels[0])):
        #         if prediction[i][j] == 1:
        #             label_pos = j
        #             for key, value in feature_dict_short.items():
        #                 if value == label_pos:
        #                     style_pred_i.append(key)
        #                     style_pred_i.append(str(CLASS[i][j])+"%")

        #     STYLE_PRED.append(type_pred_i)

        # print(STYLE_PRED)
        # print(STYLE)

        # for i in range(len(index_testing)):
        #     # STYLE_PRED[i].sort()
        #     if len(STYLE_PRED[i]) >= 1:
        #         STYLE_PRED[i] = ", ".join(STYLE_PRED[i])
        #     else:
        #         STYLE_PRED[i] = ""

        #     STYLE[i].sort()
        #     if len(STYLE[i]) >= 1:
        #         STYLE[i] = ", ".join(STYLE[i])
        #     else:
        #         STYLE[i] = ""

        # print(STYLE_PRED)
        # print(STYLE)
        # print(CLASS)

        # table = pd.DataFrame({"OBJECT_ID": ID, "URL": URL, "STYLE": STYLE,
        #                       "STYLE PREDICTION": STYLE_PRED, "SCORE": SCORE})

        # table.to_csv(cwd + "/output_tables/" + "style_pred.csv")

        path = cwd + model_weight_path_2 + 'nm_pred/' + lsb_type + "/"
        try:
            os.makedirs(path)
            saver = tf.train.Saver()
            saver.save(sess, path + "pattern_model")
        except:
            saver = tf.train.Saver()
            saver.save(sess, path + "pattern_model")

    tf.reset_default_graph()
