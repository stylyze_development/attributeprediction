import csv
import numpy as np
import logging
import os
import pandas as pd
import pickle
import re
import copy
from datetime import datetime

# TODO: implement some logging functions to replace the print functions

# logging.basicConfig(level=logging.INFO)


def load_data(database_path, output_table_path, column_names, lsb_type, item_counter):
    """

    """

    startTime = datetime.now()
    # Load items in the database (big table)
    cwd = os.getcwd()
    df = pd.read_csv(cwd + database_path)

    # df_homedepot = pd.read_csv(cwd + "/data_tables/VISUALLY_SIMILAR_HOMEDEPOT_QA_APPROVED_EXTENDED_20181206_CV.csv")
    # print("**************************************" + str(len(df)))
    # print("**************************************" + str(len(df_homedepot)))
    # df = df.append(df_homedepot, ignore_index=True)
    # df = df.reset_index(drop=True)

    # print("************************************" + str(len(df)))

    # df = df.loc[:700000]

    df_index_0 = df.index.values.tolist()

    df_length = len(df)

    # Rename the columns
    df = df.rename(columns={column_names[0]: "OBJECT_ID",
                            column_names[1]: "TYPES",
                            column_names[2]: "STYLES",
                            column_names[3]: "CATEGORIES",
                            column_names[4]: "CURATOR_IMAGE_PATH",
                            column_names[5]: "IMAGE_URL",
                            column_names[6]: "PATTERNS"
                            })

    # Pull the columns needed from the big table
    df = df[["OBJECT_ID", "CATEGORIES", "TYPES", "STYLES", "CURATOR_IMAGE_PATH", "IMAGE_URL", "ARCHIVED", "QA_APPROVED", "STYLE_LAST_OVERRIDE_USER_ID", "STYLES_LAST_OVERRIDDEN_DATE_USER", "STYLES_LAST_OVERRIDE_USER_VALUE"]]
    df = df.fillna("Missing") 
    # df = df[df["ARCHIVED"] != 1]
    df = df[(df["STYLE_LAST_OVERRIDE_USER_ID"] == "lperrone@stylyze.com") | (df["STYLE_LAST_OVERRIDE_USER_ID"] == "aheugly@stylyze.com")]
    df = df[df["STYLES_LAST_OVERRIDDEN_DATE_USER"] < "2019-10-01T01:50:45.320Z"] 

    # df["STYLE_SCORE_2"] = df["STYLE_SCORE"]
    # def parser_5(str_input):
    #     if str_input != "Missing":
    #         a = re.split('[|]', str_input)
    #         a.sort()
    #         return a[0] 
    #     else:
    #         return "Missing"
    # df["STYLE_SCORE_2"] = df.apply(lambda row: parser_5(row["STYLE_SCORE_2"]), axis=1) 
    # # df = df[(df["STYLE_SCORE_2"] == "9") | (df["STYLE_SCORE_2"] == "8") | (df["STYLE_SCORE_2"] == "Missing")]
    # df = df[(df["STYLE_SCORE_2"] == "9") | (df["STYLE_SCORE_2"] == "8")]

    # The following functions are used to parse the type and style attributes
    def parser(str_input):
        if str_input != "Missing":
            # a = str_input.replace(" ", "")
            a = re.split('[;]', str_input)
            a.sort()
            a = ",".join(a)
            return a
        else:
            return "Missing"

    def parser_2(str_input):
        if str_input != "Missing":
            # a = str_input.replace(" ", "")
            a = re.split('[;]', str_input)
            a.sort()
            # a = a[:1]
            # a = [", ".join(a)]
            return a
        else:
            return "Missing"

    def parser_3(str_input):
        if str_input != "Missing":
            # a = str_input.replace(" ", "")
            a = re.split('[,]', str_input)
            a.sort()
            a = a[:1]
            a = [", ".join(a)]
            return a
        else:
            return "Missing"           
 
    # Apply the parsers to the types and styles
    df["TYPES"] = df.apply(lambda row: parser(row["TYPES"]), axis=1)
    df["STYLES"] = df.apply(lambda row: parser_2(row["STYLES"]), axis=1)
    df["STYLES_LAST_OVERRIDE_USER_VALUE"] = df.apply(lambda row: parser_3(row["STYLES_LAST_OVERRIDE_USER_VALUE"]), axis=1)
    # df["CATEGORIES"] = df.apply(lambda row: parser(row["CATEGORIES"]), axis=1)
    # df["PATTERNS"] = df.apply(lambda row: parser_3(row["PATTERNS"]), axis=1)

    df_big = df
    # df_big["VARIANT_ID"] = df_big["VARIANT_ID"].astype('int64')

    # Drop the items with missing and/or uncategorized type and/or style
    df_big = df_big[df_big["STYLES"] != "Missing"]
    # df_big = df_big[df_big["TYPES"] != "Missing"]
    df_big = df_big[df_big["STYLES"] != "Uncategorized"]
    # df_big = df_big[df_big["TYPES"] != "uncategorized"]
    # df_big = df_big[df_big["PATTERNS"] != "Missing"]

    df_big = df_big[df_big["STYLES_LAST_OVERRIDE_USER_VALUE"] != "Missing"]
    df_big = df_big[df_big["STYLES_LAST_OVERRIDE_USER_VALUE"] != "Uncategorized"]

    # df_big = df_big[df_big["CATEGORIES"] == "Blouses"]
    # df_big = df_big[df_big["CATEGORIES"] == "Bags"]

    df_big = df_big[(df_big["CATEGORIES"] == "Blouses") | (df_big["CATEGORIES"] == "Tank Tops")]

    # df_big = df_big[df_big["TYPES"] == "area rug"]

    # df_big = df_big[(df_big["TYPES"] == "area rug") | (df_big["TYPES"] == "kitchen rug") | (df_big["TYPES"] == "outdoor area rug") |
    #                 (df_big["TYPES"] == "outdoor rugs") | (df_big["TYPES"] == "round rug") | (df_big["TYPES"] == "outdoor runner") |
    #                 (df_big["TYPES"] == "runner") | (df_big["TYPES"] == "accent rug")]

    # df_big = df_big[(df_big["TYPES"] == "decorative pillow")]

    # df_big = df_big[(df_big["TYPES"] == "wall art") | (df_big["TYPES"] == "wall decor") | (df_big["TYPES"] == "bathroom wall art") |
    #                 (df_big["TYPES"] == "kitchen wall art") | (df_big["TYPES"] == "sports wall art") | (df_big["TYPES"] == "wall art items")]

    # df_big = df_big[(df_big["TYPES"] == "bedroom set") | (df_big["TYPES"] == "bed") | (df_big["TYPES"] == "bunk bed") |
    #                 (df_big["TYPES"] == "guest bed") | (df_big["TYPES"] == "daybed")]

    # df_big = df_big[(df_big["TYPES"] == "headborad")] 

    print("*******************************************")
    print("*******************************************")
    print("*******************************************")
    print("GROUP: " + str(item_counter))

    print("TOTAL NUMBER OF LSBS:" + str(len(df_big)))

    # print(df_big["STYLES"])
    # print(df_big["CURATOR_IMAGE_PATH"])
    # print(df_big["CURATOR_IMAGE_PATH"][50905])
    # print(df_big["TYPES"][1307])
    # print(df_big["TYPES"][76][0])
    # print(df_big["TYPES"][76][1])

    type_array = df_big["STYLES_LAST_OVERRIDE_USER_VALUE"]
    # type_array = df_big["TYPES"]
    # type_array = df_big["STYLES"]
    # type_array = df_big["PATTERNS"]
    img_path = df_big["CURATOR_IMAGE_PATH"]

    df_index = df_big.index.values.tolist()


    # startTime = datetime.now()
    max_length  = len(max(type_array, key=len))
    # time_used = datetime.now() - startTime
    # print("TIME TAKEN:", time_used)
    
    # startTime = datetime.now()

    # type_a = [["" for x in range(len(max(type_array, key=len)))]
    #           for x in range(df_length)]

    # time_used = datetime.now() - startTime
    # print("TIME TAKEN:", time_used)  
    
    # startTime = datetime.now()        

    type_a = [[""]*max_length for x in range(df_length)]

    # time_used = datetime.now() - startTime
    # print("TIME TAKEN:", time_used) 

    img_a = ["" for x in range(df_length)]

    # time_used = datetime.now() - startTime
    # print("TIME TAKEN:", time_used)

    for i in df_index:
        for j in range(len(type_array[i])):
            if type_array[i][j]:
                type_a[i][j] = type_array[i][j]
                img_a[i] = img_path[i]
            else:
                type_a[i][j] = ""
                img_a[i] = ""           

    type_a = np.asarray(type_a)

    print("DIMENSION OF LABELS:" + " " + str(len(type_a[0])))
    # print(len(type_a))
    # print(img_a)
    # print(len(img_a))
    # print(df_length)
    # print(len(img_a[0]))

    if len(type_a[0]) >= 2:

        number_query_example = df_length
        labels = {"df_index": df_index,
                  "img_path": img_a,
                  "type_1": type_a[:, 0].reshape(number_query_example, 1),
                  "type_2": type_a[:, 1].reshape(number_query_example, 1),
                  # "type_3": type_a[:, 2].reshape(number_query_example, 1),
                  # "type_4": type_a[:, 3].reshape(number_query_example, 1),
                  # "type_5": type_a[:, 4].reshape(number_query_example, 1),
                  # "type_6": type_a[:, 5].reshape(number_query_example, 1),
                  # "type_7": type_a[:, 6].reshape(number_query_example, 1),
                  # "type_8": type_a[:, 7].reshape(number_query_example, 1),
                  # "type_9": type_a[:, 8].reshape(number_query_example, 1),
                  # "type_10": type_a[:, 9].reshape(number_query_example, 1),
                  # "type_11": type_a[:, 10].reshape(number_query_example, 1),
                  # "type_12": type_a[:, 11].reshape(number_query_example, 1),
                  # "type_13": type_a[:, 12].reshape(number_query_example, 1),
                  # "type_14": type_a[:, 13].reshape(number_query_example, 1),
                  # "type_15": type_a[:, 14].reshape(number_query_example, 1)
                  }

        # print(len(labels["df_index"]))
        # print(img_a[34])
        # print(img_a[76])
        # print(labels.keys())

        number_tasks = 2

        # X = labels["type_1"]

        X = np.concatenate((labels["type_1"],
                            labels["type_2"]),axis=1)
                            # labels["type_3"],
                            # labels["type_4"]),axis=1)
                            # labels["type_5"], 
                            # labels["type_6"],  
                            # labels["type_7"], 
                            # labels["type_8"]),
                            # labels["type_9"],
                            # labels["type_10"],
                            # labels["type_11"],
                            # labels["type_12"],
                            # labels["type_13"],
                            # labels["type_14"],
                            # labels["type_15"]), axis=1)

        # print(X[:5])

        u_type_1 = np.unique(labels["type_1"])
        u_type_2 = np.unique(labels["type_2"])
        # u_type_3 = np.unique(labels["type_3"])
        # u_type_4 = np.unique(labels["type_4"])
        # u_type_5 = np.unique(labels["type_5"])
        # u_type_6 = np.unique(labels["type_6"])
        # u_type_7 = np.unique(labels["type_7"])
        # u_type_8 = np.unique(labels["type_8"])
        # u_type_9 = np.unique(labels["type_9"])
        # u_type_10 = np.unique(labels["type_10"])
        # u_type_11 = np.unique(labels["type_11"])
        # u_type_12 = np.unique(labels["type_12"])
        # u_type_13 = np.unique(labels["type_13"])
        # u_type_14 = np.unique(labels["type_14"])
        # u_type_15 = np.unique(labels["type_15"])

        # u_features = u_type_1

        u_features = np.unique(np.concatenate((u_type_1,
                                               u_type_2,)))
                                               # u_type_3,
                                               # u_type_4,)))
                                               # u_type_5,
                                               # u_type_6,
                                               # u_type_7, 
                                               # u_type_8,))) 
                                               # u_type_9,
                                               # u_type_10,
                                               # u_type_11,
                                               # u_type_12, 
                                               # u_type_13,
                                               # u_type_14,
                                               # u_type_15)))

    else:
        number_query_example = df_length
        labels = {"df_index": df_index,
                  "img_path": img_a,
                  "type_1": type_a[:, 0].reshape(number_query_example, 1)}

        number_tasks = 1

        X = labels["type_1"]
        u_type_1 = np.unique(labels["type_1"])
        u_features = u_type_1

    number_unique_features = len(u_features)

    i = 0
    feature_dict = {}
    for item in u_features:
        feature_dict[item] = i
        i += 1

    print(feature_dict)
    print("NUMBER OF UNIQUE LABELS:" + " " + str(number_unique_features))

    y_labels = np.zeros((number_query_example, number_unique_features))
    counter = np.zeros((number_unique_features))
    for i in range(number_query_example):
        for j in range(number_tasks):
            if X[i, j] in feature_dict.keys():
                index = feature_dict[X[i, j]]
                y_labels[i, index] = 1
                counter[index] += 1 / (number_query_example * number_tasks)

    # print(y_labels[0, :])
    # print(y_labels[100, :])

    # print(set(counter.argsort()[-22:][::-1]))
    # print(counter)
    # print(counter.argsort()[-20:][::-1])

    # for value, key in feature_dict.items():
    #     if key in counter.argsort()[-22:][::-1]:
    #         print(key)
    #         print(value)

    # feature_dict_short = {'blockheelcagesandals': 0, 'a-lineballskirt': 1, 'a-linecocktaildress': 2, 'a-linecoldshoulderdress': 3, 'a-linecrepedress': 4, 'a-linedenimskirt': 5, 'a-linedress': 6, 'a-linegown': 7, 'a-linehalterdress': 8, 'a-lineknee-lengthskirt': 9, 'a-linelacedress': 10, 'a-linelacemidiskirt': 11, 'a-lineleathermidiskirt': 12, 'a-linemaxidress': 13, 'a-linemidiskirt': 14, 'a-lineminiskirt': 15, 'a-lineshirtdress': 16, 'a-lineskirt': 17, 'a-linetulleskirt': 18, 'a-linewoolskirt': 19, 'a-linewrapdress': 20, 'a-linewrapskirt': 21, 'activebackpack': 22, 'activecroptop': 23, 'activeduffelbag': 24, 'activejacket': 25, 'activeracerbacktank': 26, 'activetank': 27, 'anklebooties': 28, 'ankleboyfriendjeans': 29, 'anklejeanleggings': 30, 'anklejeans': 31, 'anklepants': 32, 'anklepantsleggings': 33, 'anklestrapblockheelpumps': 34, 'anklestrapcagesandals': 35, 'anklestrapdorsaypumps': 36, 'anklestrapespadrillesandals': 37, 'anklestrapflatsandals': 38, 'anklestrappeeptoeflats': 39, 'anklestrappeeptoepumps': 40, 'anklestrapplatformespadrillesandals': 41, 'anklestrappointedtoepumps': 42, 'anklestrappumps': 43, 'anklestrapwedgepumps': 44, 'anklestrapwedgesandals': 45, 'anklet': 46, 'ankletieflatsandals': 47, 'ankletielaceupsandals': 48,
    #                       'ankletiesandals': 49, 'ankletiewedgesandals': 50, 'ankletrousers': 51, 'ankleworkoutleggings': 52, 'asymmetricalhemtop': 53, 'athleisurejumpsuit': 54, 'athleisurevest': 55, 'athleticcroptop': 56, 'babydolldress': 57, 'backpack': 58, 'bagstrap': 59, 'balletflats': 60, 'balletnecktee': 61, 'balletneckthreequartersleevetop': 62, 'balletwedgesandals': 63, 'ballskirt': 64, 'bandeaubikinitop': 65, 'bandeauone-piece': 66, 'bangle': 67, 'barnecklace': 68, 'baseballtee': 69, 'bateaunecktop': 70, 'batwingtop': 71, 'beachbag': 72, 'bellsleeveblouse': 73, 'bellsleevebody-condress': 74, 'bellsleevecardigan': 75, 'bellsleevecroptop': 76, 'bellsleevedress': 77, 'bellsleeveminidress': 78, 'bellsleevepeasantblouse': 79, 'bellsleevepeplumblouse': 80, 'bellsleevepoplinblouse': 81, 'bellsleeveshiftdress': 82, 'bellsleevesurpliceblouse': 83, 'bellsleevetop': 84, 'bellsleevetunic': 85, 'beltbag': 86, 'belteda-linedress': 87, 'belteddress': 88, 'beltedprintchiffonmaxidress': 89, 'beltedsheathdress': 90, 'beltedsleevelessdress': 91, 'bermudashorts': 92, 'bikinibottoms': 93, 'bikinitop': 94, 'birkenstocksandals': 95, 'blazer': 96, 'blazerdress': 97, 'blazersuitjacketcoat': 98, 'blazervest': 99, 'blockheelbooties': 100}

    # feature_dict_short = {"pendantnecklace": 0, 'dropearrings': 1, 'bracelet': 2, 'diamondring': 3, 'necklace': 4, 'hoopearrings': 5,
    #                       "shoulderbag": 6, "tote": 7, "crossbodybag": 8, "sundress": 9, "v-neckcroptop": 10, "top": 11}

    # feature_dict_short = {'Artsy': 0, 'Basics': 1, 'Bohemian': 2, 'Chic': 3, 'Classic': 4, 'Edgy': 5, 'Outdoorsy': 6, 'Preppy': 7, 'Romantic': 8, 'StreetStyle': 9}

    # feature_dict_short =  {'Contemporary': 0, 'Farmhouse': 1, 'Industrial': 2, 'Mid-CenturyModern': 3, 'Modern': 4, 'Rustic': 5, 'ShabbyChic': 6, 'Traditional': 7, 'Urban': 8, 'Vintage': 9}

    # feature_dict_short = {'ArtDeco': 0, 'Bohemian': 1, 'Classic': 2, 'Coastal': 3, 'Cottage': 4, 'Farmhouse': 5,
    #                       'Glam': 6, 'Industrial': 7, 'Mediterranean': 8, 'Mid-CenturyModern': 9, 'Minimalist': 10,
    #                       'Mission': 11, 'Modern': 12, 'Rustic': 13, 'Southwestern': 14, 'Transitional': 15} 

    # print(feature_dict_short)                      

    feature_dict_short_0 = copy.deepcopy(feature_dict)
    key_items = list(feature_dict.keys())
    del feature_dict_short_0['']
    # del feature_dict_short_0['default']
    if 'Uncategorized' in key_items: 
        del feature_dict_short_0['Uncategorized']
    if 'uncategorized' in key_items: 
        del feature_dict_short_0['uncategorized'] 
    if 'default' in key_items: 
        del feature_dict_short_0['default']       
    print(feature_dict_short_0)    

    feature_dict_short = {}

    feature_dict_short_i = 0
    for key, value in feature_dict_short_0.items():
        # feature_dict_short[key] = value - 1
        feature_dict_short[key] = feature_dict_short_i
        feature_dict_short_i += 1

    print(feature_dict_short)    

    X_short = np.zeros((X.shape))
    for i in range(number_query_example):
        for j in range(number_tasks):
            if X[i, j] in feature_dict_short.keys():
                X_short[i, j] = feature_dict[X[i, j]]
            else:
                X_short[i, j] = 0

    # print(X_short[34])
    # print(X_short)

    feature_length = len(np.unique(X_short)) - 1
    print("NUMBER OF CLASSES:" + " " + str(feature_length))

    X_short_2 = X_short.astype(int)
    # print(X_short_2[34])

    X_short_3 = [["" for x in range(number_tasks)]
                 for x in range(number_query_example)]
    for i in range(number_query_example):
        for j in range(number_tasks):
            for key, value in feature_dict.items():
                if value == X_short_2[i, j]:
                    X_short_3[i][j] = key

    X_short_3 = np.array(X_short_3)
    # print(X_short_3[0])
    # print(X_short_3[34])

    y_labels_2 = np.zeros((number_query_example, feature_length))
    counter_2 = np.zeros((feature_length))
    for i in range(number_query_example):
        for j in range(number_tasks):
            if X_short_3[i, j] in feature_dict_short.keys():
                index = feature_dict_short[X[i, j]]
                y_labels_2[i, index] = 1
                counter_2[index] += 1 / (number_query_example * number_tasks)

    # print(y_labels_2[0, :])
    # print(y_labels_2[34, :])

    list_index = []
    for i in range(number_query_example):
        if np.sum(y_labels_2[i]) != 0:
            list_index.append(df_index_0[i])

    # print(list_index)

    y_labels_3 = []
    for i in range(number_query_example):
        if np.sum(y_labels_2[i]) != 0:
            y_labels_3.append(y_labels_2[i])

    # print(y_labels_3)

    y_labels_3 = np.array(y_labels_3)
    # print(y_labels_3)
    # print(y_labels_3[0])

    for i in range(len(y_labels_3)):
        if np.sum(y_labels_3[i]) == 0:
            print("Ha")

    list_index = np.array(list_index)
    list_index = np.expand_dims(list_index, axis=1)
    print("TOTAL NUMBER OF SAMPLES (TRAINING):" + " " + str(list_index.shape[0]))
    # print(y_labels_3.shape)
    y_index = np.concatenate((list_index, y_labels_3), axis=1)
    # print(y_index)
    y_index = y_index.astype(int)
    # print(y_index[:10, 1:])

    number_items = int(len(y_index)/320)*320
    indices = np.random.permutation(number_items) 
    y_index = y_index[indices]

    class_stats = (np.sum(y_index[:, 1:], axis=0)) / len(y_index)
    print("STYLE DISTRIBUTION:")
    print(class_stats)
    print(np.sum(class_stats))

    # target_class = []
    # homedepot_class = []
     
    # for i in range(len(y_index)):
    #     for j in range(len(class_stats)):
    #         if y_index[i, j+1] == 1 and j in homedepot_class:
    #             y_index[i, j+1] = 0
                
    # target_class = []
    # y_index_updated_0 = []
    # for i in range(len(y_index)):
    #     for j in range(len(class_stats)):
    #         if y_index[i, j+1] == 1 and j in target_class:
    #             y_index_updated_0.append(y_index[i])            

    # y_index_updated_0 = np.array(y_index_updated_0)
    # y_index = y_index_updated_0
    # class_stats = (np.sum(y_index[:, 1:], axis=0)) / len(y_index)
    # print("UPDATED STYLE DISTRIBUTION:")
    # print(class_stats)
    # print(np.sum(class_stats))

    cwd = os.getcwd()

    n_y = len(y_index[0]) - 1

    model_input_path = cwd + output_table_path
    # os.chmod(model_input_path, 0o644)
    with open(model_input_path, "wb") as my_file:
        pickle.dump(df_big, my_file, protocol=2)

    with open(cwd + "/output_tables/y_index", "wb") as my_file2:
        pickle.dump(y_index, my_file2, protocol=2)

    with open(cwd + "/output_tables/labels", "wb") as my_file3:
        pickle.dump(labels, my_file3, protocol=2)

    path = cwd + "/output_tables/" + 'nm/' + lsb_type + "/"
    try:  
        os.makedirs(path)
        with open(path + "n_y", "wb") as my_file4:
            pickle.dump(n_y, my_file4, protocol=2)  
    except:
        with open(path + "n_y", "wb") as my_file4:
            pickle.dump(n_y, my_file4, protocol=2)  
         
    path = cwd + "/output_tables/" + 'nm/' + lsb_type + "/"
    try:  
        os.makedirs(path)
        with open(path + "feature_dict_short", "wb") as my_file5:
            pickle.dump(feature_dict_short, my_file5, protocol=2)  
    except:
        with open(path + "feature_dict_short", "wb") as my_file5:
            pickle.dump(feature_dict_short, my_file5, protocol=2)
         
    time_used = datetime.now() - startTime
    # print("EPOCH:" + " " + str(epoch))
    print("TIME TAKEN:", time_used)

    return n_y, feature_dict_short, class_stats
