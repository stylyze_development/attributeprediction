import csv
import cv2
from datetime import datetime
import logging
import numpy as np
import os
import pandas as pd
import pickle
import scipy.io
import tensorflow as tf

from . import generator3 as generator
from . import models3 as models


def embedding(model_input_path_2,
              n_H0, n_W0, n_C0,
              batch_size, training_ratio, embedding_size,
              image_normalization,
              same_image_epsilon,
              model_weight_path,
              model_weight_path_2,
              model_name,
              output_layer,
              num_epoch,
              MARGIN,
              net_3_dim,
              learning_rate,
              keepratio,
              n_y,
              feature_dict_short,
              lsb_type):
    """

    """

    cwd = os.getcwd()

    with open(cwd + "/output_tables/y_index", "rb") as my_file:
        y_index = pickle.load(my_file)

    with open(cwd + model_input_path_2, "rb") as my_file2:
        deep_learning_df = pickle.load(my_file2)

    with open(cwd + "/output_tables/labels", "rb") as my_file3:
        labels = pickle.load(my_file3)

    image_path = labels["img_path"]

    index = y_index[:, 0]
    y_labels = y_index[:, 1:]

    print("NUMBER OF SAMPLES (INFERENCE):", len(y_labels))
    print("NUMBER OF CLASSES (INFERENCE):", len(y_labels[0]))

    print("*******************************************")
    # print("***TOTAL NUMBER OF LSBS:" + str(len(y_labels)))

    grouped = deep_learning_df

    num_epoch = 1
    batch_size = 1
    training_ratio = 1.0
    n_H0, n_W0, n_C0 = (224, 224, 3)
    net_3_dim = net_3_dim
     
    # Parameters
    params = {'dim_x': n_W0,
              'dim_y': n_H0,
              'dim_z': n_C0,
              'batch_size': batch_size,
              'n_y': n_y,
              'shuffle': False}

    total_bad_imgs = 0
    number_of_products = len(y_labels)
    m = number_of_products

    y_labels_training = {}
    partition_training = []
    index_training = []
    for i in range(0, int(number_of_products * training_ratio)):
        index_i = int(index[i])
        y_labels_training[str(index_i)] = y_labels[i, :]
        partition_training.append(str(index_i))
        index_training.append(index_i)

    training_generator = generator.DataGenerator(
        **params).generate(y_labels_training, partition_training, image_path)

    minibatch_size = batch_size
    num_minibatches_training = np.int(np.round(m * training_ratio) / minibatch_size)
    
    init = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init)
        path = cwd + model_weight_path_2 + 'nm_pred/' + lsb_type + "/"
        new_saver = tf.train.import_meta_graph(path + 'pattern_model.meta')
        new_saver.restore(sess, tf.train.latest_checkpoint(path))
        graph = tf.get_default_graph()
        # tensors = [n.name for n in tf.get_default_graph().as_graph_def().node]
        # print(tensors)

        img_placeholder = graph.get_tensor_by_name("img_placeholder:0")
        x = graph.get_tensor_by_name("x:0")
        y = graph.get_tensor_by_name("y:0")
        keepratio = graph.get_tensor_by_name("keepratio:0")
        train_features_tf = graph.get_tensor_by_name("train_features_tf:0")
        Z33 = graph.get_tensor_by_name("Z33:0")
        class_dist = graph.get_tensor_by_name("class_dist:0") 

        print("*******************************************")
        print("***INFERENCE BEGINS...")
        startTime = datetime.now()
        for epoch in range(num_epoch):
            group_i = grouped
            if len(group_i) >= 1:
                minibatches_training = training_generator
                matrix_res = np.zeros([np.int(np.round(m * training_ratio)), 7 * 7 * 512])
                prediction_train = np.zeros((np.int(np.round(m * (training_ratio))), n_y))
                y_train_true = np.zeros((np.int(np.round(m * (training_ratio))), n_y))
                class_dist_np = np.zeros((np.int(np.round(m * (training_ratio))), n_y))
                for i in range(num_minibatches_training):
                    (minibatch_X_training, minibatch_Y_training, bad_img) = next(minibatches_training)

                    total_bad_imgs += np.sum(bad_img)

                    train_features_np = train_features_tf.eval(feed_dict={img_placeholder: minibatch_X_training})

                    ntrain = batch_size
                    train_vectorized = np.ndarray((ntrain, 7 * 7 * 512))

                    for j in range(ntrain):
                        curr_feat = train_features_np[j, :, :, :]
                        curr_feat_vec = np.reshape(curr_feat, (1, -1))
                        train_vectorized[j, :] = curr_feat_vec

                    matrix_res[i * batch_size:(i + 1) * batch_size, :] = train_vectorized

                    batch_xs = train_vectorized
                    batch_ys = minibatch_Y_training

                    class_dist_np[i * batch_size:(i + 1) * batch_size, :] = class_dist.eval(feed_dict={x: batch_xs, y: batch_ys, keepratio: 1.})
                     
                    prediction_train[i * batch_size:(i + 1) * batch_size, :] = Z33.eval(feed_dict={x: batch_xs, y: batch_ys, keepratio: 1.})

                    y_train_true[i * batch_size:(i + 1) * batch_size, :] = batch_ys

                train_acc_estimate = np.mean((np.array(prediction_train) == np.array(y_train_true)).astype(float), axis=0)
                train_acc = (np.sum(train_acc_estimate) / n_y)

                # print(np.sum(prediction_train, axis=0)/10001)
                # print(prediction_train[:100])
                # print(y_train_true[:100])
                # print(train_acc_estimate)

                time_used = datetime.now() - startTime
                # print("ACCURACY: " + str(train_acc))
                # print("COST: " + str(minibatch_cost_testing))
                SCORE = []
                NULL = []
                for i in range(len(index_training)):
                    # num_items = sum(y_test_true[i])
                    num_items = 0
                    score_i = 0
                    for j in range(n_y):
                        if y_train_true[i][j] == 1 and prediction_train[i][j] == 1:
                            num_items += 1
                            score_i += 1
                        elif y_train_true[i][j] == 0 and prediction_train[i][j] == 1:
                            num_items += 1
                        elif y_train_true[i][j] == 1 and prediction_train[i][j] == 0:
                            num_items += 1  
                    score_i = score_i/num_items          
                                   
                    SCORE.append(round(score_i, 3))

                    if np.sum(prediction_train[i]) == 0:
                        NULL.append(1)
                    else:
                        NULL.append(0)

                AVG_NULL = np.mean(NULL)    
                AVG_SCORE = np.mean(SCORE)
                print("SCORE: " + str(AVG_SCORE))
                print("NULL PREDICTIONS: " + str(AVG_NULL))
                print("TOTAL NUMBER OF BAD IMAGES (INFERENCE):" + " " + str(int(total_bad_imgs)))
                print("TIME TAKEN:", time_used)
    
    tf.reset_default_graph()

    INDEX = []
    STYLE = []
    TRUE_STYLE = []
    URL = []
    ID = []
    STYLE_PRED = []
    CLASS = []
    STYLE_OTHER = []
    USER_ID = []
    CATEGORY = []

    for i in range(len(index_training)):
        index_i = index_training[i]
        INDEX.append(index_i)
        style_i = deep_learning_df.loc[index_i]["STYLYZE STYLES"]
        # style_i = deep_learning_df.loc[index_i]["STYLES_LAST_OVERRIDES_2"]
        STYLE.append(style_i)
        style_other_i = deep_learning_df.loc[index_i]["STYLE OTHER"]
        STYLE_OTHER.append(style_other_i)
        url_i = deep_learning_df.loc[index_i]["IMAGE_URL"]
        URL.append(url_i)
        id_i = deep_learning_df.loc[index_i]["OBJECT_ID"]
        ID.append(str(id_i))
        category_i = deep_learning_df.loc[index_i]["CATEGORIES"]
        CATEGORY.append(str(category_i))
        user_id_i = deep_learning_df.loc[index_i]["STYLE_LAST_OVERRIDE_USER_ID"]
        USER_ID.append(str(user_id_i))


        CLASS.append([int(elem*100)for elem in class_dist_np[i].tolist()])

        style_pred_i = []

        for j in range(len(y_labels[0])):
            if prediction_train[i][j] == 1:
                label_pos = j                   
                for key, value in feature_dict_short.items():
                    if value == label_pos:
                        style_pred_i.append(key)
                        style_pred_i.append(str(CLASS[i][j])+"%")

        STYLE_PRED.append(style_pred_i)

    for i in range(len(index_training)):
        # STYLE_PRED[i].sort()
        if len(STYLE_PRED[i]) >= 1:
            # STYLE_PRED[i] = ", ".join(STYLE_PRED[i])
            STYLE_PRED[i] = ";".join(STYLE_PRED[i])
        else:
            STYLE_PRED[i] = ""

        # STYLE[i].sort()
        if len(STYLE[i]) >= 1:
            # STYLE[i] = ", ".join(STYLE[i])
            STYLE[i] = ";".join(STYLE[i])
        else:
            STYLE[i] = ""

        # STYLE[i].sort()
        if len(STYLE_OTHER[i]) >= 1:
            # STYLE[i] = ", ".join(STYLE[i])
            STYLE_OTHER[i] = ";".join(STYLE_OTHER[i])
        else:
            STYLE_OTHER[i] = ""    

    # table = pd.DataFrame({"OBJECT_ID": ID, "URL": URL, "STYLE": STYLE, "STYLE PREDICTION": STYLE_PRED, "TRUE STYLE": TRUE_STYLE})
    table = pd.DataFrame({"CATEGORY": CATEGORY, "OBJECT_ID": ID, "URL": URL, "STYLE": STYLE, "STYLE PREDICTION": STYLE_PRED, "STYLE OTHER": STYLE_OTHER, "USER ID": USER_ID})
    table.to_csv(cwd + "/weights/" + "target_pred/" + lsb_type + "/style_pred_score.csv") 
    csv_path = cwd + "/weights/" + "target_pred/" + lsb_type + "/style_pred_score.csv"

    return csv_path           
